import 'dart:io';

import 'package:corap/dao/query/user_query.dart';
import 'package:corap/model/convert/user_convertor.dart';
import 'package:corap/model/user.dart';
import 'package:corap/service/code/dao_code.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
class UserDao{
  Database? _database;
  UserConvertor userConvertor=UserConvertor();
  Future<Database?> get database async{
    if(_database==null){
      _database=await  _init();
    }
    return _database;
  }
  Future<Database> _init() async{
    String path=join(await getDatabasesPath(),"corapApp.db");
    var etradeDb=await openDatabase(path,version:1,onCreate:_createDb);
    return etradeDb;
  }
  void _createDb(Database db, int version)async{
    await db.execute(UserQuery.CREATE);
  }
  Future<dynamic> findUser()async{
    Database? db=await this.database;
    var result=await db!.query("_user");
    if(result.isEmpty){
      return DaoCode.NO_USER;
    }
    return userConvertor.jsonToUser(result.first);
  }
  Future insert(User user)async{
    Database? db=await this.database;
    var result=await db!.query("_user");
    if(!result.isEmpty){
      //TODO sadece bir kullanıcı kayıt edilebilir!
      db.rawQuery("delete from _user");
    }
    await db.insert("_user",userConvertor.userToJson(user));
  }
  Future delete()async{
    Database? db=await this.database;
    var result=await db!.query("_user");
    if(!result.isEmpty){
      await db.rawQuery("delete from _user");
    }
  }



}