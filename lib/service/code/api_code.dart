class ApiCode{

  //TODO SUCCESSFUL CODE: start 000
  static final int SUCCESSFUL_REGISTER=000;
  static final int SUCCESSFUL_LOGIN=001;
  static final int SECCESSFUL_REQUEST=002;

  //TODO ERROR CODE: start 100
  static final int ERROR_REGISTER=100;
  static final int ERROR_LOGIN=101;
  static final int ERROR_UPDATE=102;
  static final int ERROR_REQUEST=103;
  static final int ERROR_UNAUTHORIZED=104;
  static final int ERROR_FORBIDDEN=105;

  //TODO STATUS CODE: start 200
  static final int NO_INTERESTS_CATEGORY=200;
  static final int NO_INTERESTS=201;
}