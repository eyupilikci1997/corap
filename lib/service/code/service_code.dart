class ServiceCode{
  //TODO SUCCESSFUL CODE: start 000
  static final int SUCCESSFUL_REGISTER=000;
  static final int SUCCESSFUL_LOGIN=001;
  static final int SUCCESSFUL_USER_UPDATE=002;
  static final int SUCCESSFUL_REQUEST=003;

  //TODO ERROR CODE: start 100
  static final int ERROR_REGISTER=100;
  static final int ERROR_LOGIN=101;
  static final int ERROR_USER_UPDATE=102;
  static final int ERROR_REQUEST=103;
  static final int ERROR_UNAUTHORIZED=104;
  static final int ERROR_FORBIDDEN=105;


  //TODO STATUS CODE: start 200
  static final int NO_USER=200;
  static final int NO_INTERESTS=201;
}