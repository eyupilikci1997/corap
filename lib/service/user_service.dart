import 'package:corap/api/user_api.dart';
import 'package:corap/dao/user_dao.dart';
import 'package:corap/model/interests.dart';
import 'package:corap/model/user.dart';
import 'package:corap/service/code/api_code.dart';
import 'package:corap/service/code/dao_code.dart';
import 'package:corap/service/code/service_code.dart';
import 'package:logger/logger.dart';

class UserService{
  var logger = Logger();
  UserApi _userApi=UserApi();
  UserDao _userDao=UserDao();
  Future<int> register(User user)async{
      int code=await _userApi.register(user);
      if(code==ApiCode.SUCCESSFUL_REGISTER)
        return ServiceCode.SUCCESSFUL_REGISTER;
      return ServiceCode.ERROR_REGISTER;
  }
  Future<int> login(User user)async{
    dynamic code=await _userApi.login(user);
    if(code is User){
      logger.i("@@@@@@@@@@ Seccessful Login");
      logger.i("@@@@@@@@@@ DB Insert User");
      await _userDao.insert(code);
      return ServiceCode.SUCCESSFUL_LOGIN;
    }
    logger.e("@@@@@@@@@@ No user");
    return ServiceCode.ERROR_LOGIN;
  }
   Future logout()async{
     logger.e("@@@@@@@@@@ DB Delete User");
     await _userDao.delete();
   }

   Future<dynamic> findUser()async{
     User? user=await _userDao.findUser();
      if(user==null){
        logger.i("@@@@@@@@@@ DB No User");
        return ServiceCode.NO_USER;
      }
    return user;
   }

  Future<int> update(User user)async{
    User? auht=await _userDao.findUser();
     var code=await _userApi.update(user,auht!);
     if(code is User){
      if(user.id==auht.id)
        await _userDao.insert(code);
      logger.i("@@@@@@@@@@ Update User");
      return ServiceCode.SUCCESSFUL_USER_UPDATE;
   }
    logger.e("@@@@@@@@@@ Fail User Update ");
    return ServiceCode.ERROR_USER_UPDATE;
  }

    Future<int> interests(var id)async{
         var dao_code=await _userDao.findUser();
        if(dao_code==DaoCode.NO_USER)
          return ServiceCode.NO_USER;
        var api_code=await _userApi.interests(dao_code, id);
       if(api_code==ApiCode.SECCESSFUL_REQUEST)
         return ServiceCode.SUCCESSFUL_REQUEST;
      return ServiceCode.ERROR_REQUEST;
     }

   Future<dynamic> getInterestsByAuth()async{
       var doa=await _userDao.findUser();
       if(doa==DaoCode.NO_USER)
         return ServiceCode.NO_USER;
       var api_response=await _userApi.getInterests(doa);
       if(api_response==ApiCode.ERROR_UNAUTHORIZED)
         return ServiceCode.ERROR_UNAUTHORIZED;
       else if(api_response==ApiCode.ERROR_FORBIDDEN)
         return ServiceCode.ERROR_FORBIDDEN;
      else if(api_response==ApiCode.ERROR_REQUEST)
       return ServiceCode.ERROR_REQUEST;
       else if(api_response is List<Interests>)
       return api_response;
  }

}