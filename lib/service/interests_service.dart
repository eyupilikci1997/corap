
import 'package:corap/api/interests_api.dart';
import 'package:corap/model/interests.dart';
import 'package:corap/model/interests_category.dart';
import 'package:corap/service/code/api_code.dart';
import 'package:corap/service/code/service_code.dart';

class InterestsService{
  InterestsApi _interestsApi=InterestsApi();

  Future<dynamic> getAllInterests()async{
    var cat_code=await _interestsApi.getAllCategory();
    if(cat_code==ApiCode.NO_INTERESTS_CATEGORY){
      return ServiceCode.NO_INTERESTS;
    }
    var in_code=await _interestsApi.getAllInterests();
    if(in_code==ApiCode.NO_INTERESTS){
      return ServiceCode.NO_INTERESTS;
    }
    return map(cat_code, in_code);
  }
  List<Interests> map(List<InterestsCategory> catList,List<Interests> inList){
    catList.forEach((cat) {
      inList.forEach((interests) {
        if(interests.category?.id==cat.id)
          interests.category=cat;
      });
    });
    return inList;
  }
}