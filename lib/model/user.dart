import 'enum/role.dart';
import 'interests.dart';

class User{
  var id;
  var name;
  var email;
  var password;
  var city;
  var country;
  var gender;
  var iban;
  var role;
  var createDate;
  var updateDate;
  var loginDate;
  List<int>? interestsId;

  User({
      this.id,
      this.name,
      this.email,
      this.password,
      this.city,
      this.country,
      this.gender,
      this.iban,
      this.role,
      this.createDate,
      this.updateDate,
      this.loginDate});
}