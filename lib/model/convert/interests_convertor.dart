import 'package:corap/model/interests.dart';
import 'package:corap/model/interests_category.dart';

class InterestsConvertor{
  Map<String,dynamic> userToJson(Interests interests){
    return <String,dynamic>
    { "id": interests.id!=null? interests.id:null,
      "interests":interests.interests!=null? interests.interests:null,
      "categoryId":interests.category!=null?interests.category?.id:null
    };
  }
  Interests jsonToUser(Map<String,dynamic> json){
    return Interests(
      id: json["id"]!=null?json["id"]:null,
      interests: json["interests"]!=null? json["interests"]:null,
      category: json["categoryId"]!=null? InterestsCategory(id: json["caegoryId"]):null,
    );
  }
  List<Interests>? jsonToList(List<dynamic> json){
    if(json.isEmpty){
      List<Interests> list=[];
      json.forEach((element) {
        list.add(jsonToUser(element));
      });
      return list;
    }
    return null;
  }
}