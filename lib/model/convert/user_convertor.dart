import 'package:corap/model/enum/role.dart';
import 'package:corap/model/user.dart';
import 'package:flutter/foundation.dart';

/*



  var id;
  var name;
  var email;
  var password;
  var city;
  var country;
  var gender;
  var iban;
  var role;

* */
class UserConvertor{
  Map<String,dynamic> userToJson(User user){
    print(user);
    return <String,dynamic>
        { "id": user.id!=null? user.id:null,
          "name":user.name!=null? user.name:null,
          "email":user.email!=null?user.email:null,
          "password":user.password!=null?user.password:null,
          "city":user.city!=null?user.city:null,
          "country":user.country!=null?user.country:null,
          "gender":user.gender!=null?user.gender:null,
          "iban":user.iban!=null?user.iban:null,
          "role":user.role!=null?enumToString(user.role):null,
        };
  }
  User jsonToUser(Map<String,dynamic> json){
    return  User(
      id: json["id"]!=null? json["id"]:null,
      name: json["name"]!=null? json["name"]:null,
      email: json["email"]!=null? json["email"]:null,
      password: json["password"]!=null? json["password"]:null,
      city: json["city"]!=null? json["city"]:null,
      country: json["country"]!=null? json["country"]:null,
      gender: json["gender"]!=null? json["gender"]:null,
      iban: json["iban"]!=null? json["iban"]:null,
      role: json["role"]!=null? json["role"]:null,
    );
  }
  
  String enumToString(dynamic enm){
    return enm.toString().split(".").last;
  }

}