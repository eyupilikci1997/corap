
import 'package:corap/model/interests_category.dart';

class InterestsCategoryConvertor{
  Map<String,dynamic> userToJson(InterestsCategory category){
    return <String,dynamic>{
      "id": category.id!=null? category.id:null,
      "category":category.category!=null? category.category:null,
    };
  }
  InterestsCategory jsonToUser(Map<String,dynamic> json){
    return InterestsCategory(
      id: json["id"]!=null?json["id"]:null,
      category: json["category"]!=null? json["category"]:null,
    );
  }

  List<InterestsCategory>? jsonToList(List<dynamic> json){
    if(json.isEmpty){
      List<InterestsCategory> list=[];
      json.forEach((element) {
          list.add(jsonToUser(element));
      });
      return list;
    }
    return null;
  }
}