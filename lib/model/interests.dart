
import 'interests_category.dart';

class Interests{
  var id;
  var interests;
  InterestsCategory? category;

  Interests({this.id, this.interests,this.category});
}