import 'package:corap/dao/user_dao.dart';
import 'package:corap/model/enum/role.dart';
import 'package:corap/model/user.dart';
import 'package:corap/service/code/service_code.dart';
import 'package:corap/service/user_service.dart';

class UserServiceTest{

  UserService userService=UserService();


  Future loginTest()async{
    print("@@@@@@@@@@@@@@@@@@@@start login test!");

    User user=User(password: "admin123",email: "admin@corap.com");
    await userService.login(user).then((service_code) =>{
        if(service_code==ServiceCode.ERROR_LOGIN)
          print("@@@@@@@@@@@@@@@@@@@@GİRİŞ BAŞARISIZ! KULLANICI BİLGİLERİ HATALI!!")
        else if(service_code==ServiceCode.SUCCESSFUL_LOGIN)
          print("@@@@@@@@@@@@@@@@@@@@GİRİŞ BAŞARILI :)")
    } );
  }
  Future findUseTest()async{
   print("@@@@@@@@@@@@@@@@@@@@Start Test Find User");

  await userService.findUser().then((value) => {
      if(value is User)
           print("@@@@@@@@@@@@@@@@@@@@Kullanıcı Bulundu:) Email:"+value.email)
        else if(value==ServiceCode.NO_USER)
          print("@@@@@@@@@@@@@@@@@@@@Login Olmuş kullanıcı  yok!!")

    });
   }

  Future deneme()async{
    UserDao dao=UserDao();
    await dao.findUser();
    print("--------------");
   }

  Future registerTest()async{
    User user=User(password: "deneme123",email: "eyupfener07@gmail.com",role: Role.MEMBER,name: "eyupp");
    userService.register(user).then((value) =>{
          if(value==ServiceCode.ERROR_REGISTER)
            print("@@@@@@@@@@@@@@@@@@@@BU KULLANICI KAYITLI!!")
          else if(value==ServiceCode.SUCCESSFUL_REGISTER)
            print("@@@@@@@@@@@@@@@@@@@@Email adresine oanylama maili gönderildi!!")
    } );
  }
  Future registerErrorTest()async{
    User user=User(password: "deneme123",email: "admin@corap.com",role: Role.MEMBER,name: "eyupp");
    userService.register(user).then((value) =>{
      if(value==ServiceCode.ERROR_REGISTER)
        print("@@@@@@@@@@@@@@@@@@@@BU KULLANICI KAYITLI!!")
      else if(value==ServiceCode.SUCCESSFUL_REGISTER)
        print("@@@@@@@@@@@@@@@@@@@@Email adresine oanylama maili gönderildi!!")
    });
  }

}