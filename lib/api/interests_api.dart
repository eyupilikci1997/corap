import 'dart:convert';
import 'dart:io';
import 'package:corap/api/end_point.dart';
import 'package:corap/model/convert/interests_category_convertor.dart';
import 'package:corap/model/convert/interests_category_convertor.dart';
import 'package:corap/model/convert/interests_convertor.dart';
import 'package:corap/model/interests_category.dart';
import 'package:corap/service/code/api_code.dart';
import 'package:http/http.dart' as http;

class InterestsApi{
  InterestsConvertor _interestsConvertor=InterestsConvertor();
  InterestsCategoryConvertor _interestsCategoryConvertor=InterestsCategoryConvertor();

  Future<dynamic> getAllCategory()async{
    var headers=<String, String>{"Content-Type": "application/json"};
    var response=await http.get(Uri.parse(EndPoint.INTERESTS_CATEGORY),headers: headers);
    if(response.statusCode==200){
        return _interestsCategoryConvertor.jsonToList(json.decode(response.body));
    }
    return ApiCode.NO_INTERESTS_CATEGORY;
  }
  Future<dynamic> getAllInterests()async{
    var headers=<String, String>{"Content-Type": "application/json"};
    var response=await http.get(Uri.parse(EndPoint.INTERESTS),headers: headers);
    if(response.statusCode==200){
      return _interestsConvertor.jsonToList(json.decode(response.body));
    }
    return ApiCode.NO_INTERESTS_CATEGORY;
  }
}