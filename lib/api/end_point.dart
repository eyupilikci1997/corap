class EndPoint{
  static final String _DOMAIN="http://corap.herokuapp.com";
  static final String _API=_DOMAIN+"/api/v1";
  static final String LOGIN=_API+"/login";
  static final String REGISTER=_API+"/home/register";
  static final String FORGOT_PASSWORD=_API+"/home/forgot/password";
  static final String INTERESTS=_API+"/interests";
  static final String INTERESTS_CATEGORY=_API+"/interests/category";
  static final String USER=_API+"/user";
  static final String USER_INTERESTS=_API+"/user/interests";
}