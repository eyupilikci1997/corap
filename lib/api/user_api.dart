import 'dart:convert';
import 'dart:io';
import 'package:corap/api/body/api_body.dart';
import 'package:corap/api/end_point.dart';
import 'package:corap/model/convert/interests_convertor.dart';
import 'package:corap/service/code/api_code.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:corap/model/convert/user_convertor.dart';
import 'package:corap/model/user.dart';
import 'package:logger/logger.dart';

class UserApi{
  var LOGGER=Logger();
  UserConvertor _userConvertor=UserConvertor();
  InterestsConvertor _interestsConvertor=InterestsConvertor();
  ApiBody _apiBody=ApiBody();

  Future<int> register(User user)async{
    var headers=<String, String>{"Content-Type": "application/json"};
    var body=jsonEncode(_userConvertor.userToJson(user));
    var response=await http.post(Uri.parse(EndPoint.REGISTER),headers: headers,body: body);
    LOGGER.i("HTTP STATUS CODE:"+response.statusCode.toString());
    if(response.statusCode==200)
      return ApiCode.SUCCESSFUL_REGISTER;
    LOGGER.e(response.body.toString());
    return ApiCode.ERROR_REGISTER;
  }
  Future<dynamic> login(User user)async{
    var headers=<String, String>{"authorization": _getBasicAuth(user.email, user.password)};
    var response=await http.get(Uri.parse("http://corap.herokuapp.com/api/v1/login"),headers:headers);
    if(response.statusCode==200)
      return _userConvertor.jsonToUser(json.decode(response.body.toString()));
    LOGGER.w("HTTP Status Code:"+response.statusCode.toString()+" Body:"+response.body.toString());
    return ApiCode.ERROR_LOGIN;
  }

  Future<dynamic> update(User upUser,User auth)async{
    var headers=<String, String>{"Content-Type": "application/json",'authorization':_getBasicAuth(auth.email, auth.password)};
    var body=jsonEncode(_userConvertor.userToJson(upUser));
    var response=await http.put(Uri.parse(EndPoint.USER),headers: headers,body: body);
    if(response.statusCode==200)
      return _userConvertor.jsonToUser(json.decode(response.body));
    return ApiCode.ERROR_UPDATE;
  }

  Future<dynamic> interests(User auth,var id)async{
    var headers=_headers(auth);
    var body=_apiBody.buildIdRequest(id);
    var response=await http.post(Uri.parse(EndPoint.USER_INTERESTS),headers: headers,body: body);
    if(response.statusCode==200){
        return ApiCode.SECCESSFUL_REQUEST;
    }
    return ApiCode.ERROR_REQUEST;
  }

  Future<dynamic> getInterests(User auth)async{
    var headers=_headers(auth);
    var response=await http.get(Uri.parse(EndPoint.USER_INTERESTS),headers: headers);
    if(response.statusCode==200){
      return _interestsConvertor.jsonToList(json.decode(response.body));
    }
    return _readResponse(response);
  }

  Map<String,String> _headers(User? auth){
    if(auth!=null)
      return <String,String>{
        "Content-Type": "application/json",'authorization':_getBasicAuth(auth.email, auth.password)
      };
    return <String,String>{
      "Content-Type": "application/json"
    };
  }

  String _getBasicAuth(String username,String password){
    var auth=username+':'+password;
    return 'Basic '+base64Encode(utf8.encode(auth));
  }
  int _readResponse(http.Response response){
    if(response.statusCode==401)
      return ApiCode.ERROR_UNAUTHORIZED;
    else if(response.statusCode==403)
      return ApiCode.ERROR_FORBIDDEN;
    return ApiCode.ERROR_REQUEST;
  }
}